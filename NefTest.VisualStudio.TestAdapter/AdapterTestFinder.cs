﻿using NefTest.Engine;
using NefTest.Engine.Interfaces;
using System;
using System.Collections.Generic;
using System.Reflection;

namespace NefTest.VisualStudio.TestAdapter {
    public class AdapterTestFinder : ITestFinder {
        private readonly ILogger _log;
        private readonly IEngine _engine;

        private Dictionary<string, ITestAssembly> _testAssemblyCache = new Dictionary<string, ITestAssembly>();

        public AdapterTestFinder(ILogger logger, IEngine engine) {
            _log = logger;
            _engine = engine;
        }

        public IEnumerable<ITestAssembly> FindAllAvailableTestAssemblies() {
            throw new System.NotImplementedException();
        }

        public ITestAssembly GetTestAssembly(string assemblyPath) {
            if (_testAssemblyCache.ContainsKey(assemblyPath)) {
                return _testAssemblyCache[assemblyPath];
            }
            else {
                var testAssembly = _engine.Resolve<ITestAssembly>(new Dictionary<object, object>() {
                        { "assemblyPath", assemblyPath }
                    });
                _testAssemblyCache.Add(assemblyPath, testAssembly);
                return testAssembly;
            }
        }

        public ITestCase GetTestCase(string assemblyPath, string testCaseName) {
            var assembly = GetTestAssembly(assemblyPath);
            assembly.Load();
            var testClasses = assembly.GetTestableClasses();

            foreach (var testClass in testClasses) {
                var testCases = testClass.GetTestCases();
                foreach (var testCase in testCases) {
                    _log.Log($"Check {testCase.FullyQualifiedName} against {testCaseName}");
                    if (testCase.FullyQualifiedName.Equals(testCaseName))
                        return testCase;
                }
            }

            return null;
        }
    }
}