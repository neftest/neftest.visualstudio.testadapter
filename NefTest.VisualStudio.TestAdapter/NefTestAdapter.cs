﻿using Microsoft.VisualStudio.TestPlatform.ObjectModel.Adapter;
using Microsoft.VisualStudio.TestPlatform.ObjectModel.Logging;
using NefTest.Engine;
using NefTest.Engine.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace NefTest.VisualStudio.TestAdapter {
    /// <summary>
    /// loosely based on NUnitTestAdapter
    /// </summary>
    public abstract class NefTestAdapter {
        /// <summary>
        /// The Uri used to identify the DecalPluginTest Executor.
        /// </summary>
        public const string ExecutorUri = "executor://NefTestExecutor";

        public TestLogger TestLog { get; private set; }
        public string WorkDir { get; private set; }

        protected void Initialize(IDiscoveryContext context, IMessageLogger messageLogger) {
            _ = context;
            TestLog = new TestLogger(messageLogger);
        }
    }
}
