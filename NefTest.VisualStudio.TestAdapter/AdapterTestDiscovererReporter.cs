﻿using Microsoft.VisualStudio.TestPlatform.ObjectModel;
using Microsoft.VisualStudio.TestPlatform.ObjectModel.Adapter;
using NefTest.Engine.Enums;
using NefTest.Engine.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NefTest.VisualStudio.TestAdapter {
    public class AdapterTestDiscovererReporter : ITestReporter {
        private readonly ILogger _logger;
        private ITestCaseDiscoverySink _discoverySink;
        private string _executorUri;

        public AdapterTestDiscovererReporter(ILogger logger) {
            _logger = logger;
        }

        public void SetDiscoverySink(ITestCaseDiscoverySink discoverySink, string executorUri) {
            _discoverySink = discoverySink;
            _executorUri = executorUri;
        }

        public void Write(ITestResult testResult) {
            if (_discoverySink == null) {
                _logger.Log($"AdapterTestReporter._discoverySink was null! Make sure to set it first with AdapterTestReporter.SetDiscoverySink(discoverySink)");
                return;
            }

            foreach (var testAssembly in testResult.GetAssemblyResults()) {
                foreach (var testClass in testAssembly.GetClassResults()) {
                    foreach (var testCase in testClass.GetCaseResults()) {
                        var tc = new TestCase(testCase.FullyQualifiedName, new Uri(_executorUri), testAssembly.TestAssembly.AssemblyPath);

                        if (testCase.TestCase.Environment != Common.Enums.EnvironmentType.Any)
                            tc.Traits.Add("Env", testCase.TestCase.Environment.ToString());

                        _logger.Log($"_discoverySink.SendTestCase: {tc.FullyQualifiedName}");
                        _discoverySink.SendTestCase(tc);
                    }
                }
            }
        }
    }
}
