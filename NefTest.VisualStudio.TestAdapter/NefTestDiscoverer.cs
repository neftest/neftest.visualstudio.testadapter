﻿using Microsoft.VisualStudio.TestPlatform.ObjectModel;
using Microsoft.VisualStudio.TestPlatform.ObjectModel.Adapter;
using Microsoft.VisualStudio.TestPlatform.ObjectModel.Logging;
using NefTest.Engine;
using NefTest.Engine.Interfaces;
using NefTest.Engine.Lib;
using System;
using System.Collections.Generic;

namespace NefTest.VisualStudio.TestAdapter {
    [FileExtension(".dll")]
    [DefaultExecutorUri(ExecutorUri)]
    public sealed class NefTestDiscoverer : NefTestAdapter, ITestDiscoverer {
        private IEngine _engine;

        #region ITestDiscoverer Members
        public void DiscoverTests(IEnumerable<string> sources, IDiscoveryContext discoveryContext, IMessageLogger messageLogger, ITestCaseDiscoverySink discoverySink) {
            Initialize(discoveryContext, messageLogger);
            InitializeForExecution(discoveryContext, discoverySink);
            TestLog.Debug($"NefTest: Test discovery starting");

            try {
                var filter = new TestFilter() {
                    Environment = Common.Enums.EnvironmentType.Any,
                    AssemblyFilters = sources,
                };

                TestLog.Log($"DiscoverTest Sources: {string.Join(", ", sources)}");

                var testResult = _engine.TestRunner.FindAvailableTests(filter);
                _engine.TestReporter.Write(testResult);
            }
            catch (Exception ex) {
                TestLog.Warning("NefTest: Exception thrown discovering tests", ex);
            }

            TestLog.Debug($"NefTest: Test discovery complete");
        }

        #endregion

        #region Helper Methods
        public void InitializeForExecution(IDiscoveryContext discoveryContext, ITestCaseDiscoverySink discoverySink) {
            TestLog.Info($"NefTest: Test execution started");
            _engine = new NefTestEngine();

            _engine.RegisterComponent(typeof(ILogger), typeof(TestLogger), true, TestLog);
            _engine.RegisterComponent(typeof(ITestFinder), typeof(AdapterTestFinder), true);
            _engine.RegisterComponent(typeof(ITestReporter), typeof(AdapterTestDiscovererReporter), true);

            _engine.Initialize();

            ((AdapterTestDiscovererReporter)_engine.TestReporter).SetDiscoverySink(discoverySink, ExecutorUri);
        }
        #endregion
    }
}
