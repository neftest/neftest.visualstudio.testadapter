﻿using Microsoft.VisualStudio.TestPlatform.ObjectModel;
using Microsoft.VisualStudio.TestPlatform.ObjectModel.Adapter;
using Microsoft.VisualStudio.TestPlatform.ObjectModel.Client;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using NefTest.Engine.Enums;
using NefTest.Engine.Interfaces;
using NefTest.Engine.Lib;
using NefTest.Engine;

namespace NefTest.VisualStudio.TestAdapter {
    [ExtensionUri(ExecutorUri)]
    public sealed class NefTestExecutor : NefTestAdapter, ITestExecutor {
        private IEngine _engine;
        #region ITestExecutor Implementation

        /// <summary>
        /// Called by the Visual Studio IDE to run all tests. Also called by TFS Build
        /// to run either all or selected tests. In the latter case, a filter is provided
        /// as part of the run context.
        /// </summary>
        /// <param name="sources">Sources to be run.</param>
        /// <param name="runContext">Context to use when executing the tests.</param>
        /// <param name="frameworkHandle">Test log to send results and messages through.</param>
        public void RunTests(IEnumerable<string> sources, IRunContext runContext, IFrameworkHandle frameworkHandle) {
            Initialize(runContext, frameworkHandle);
            InitializeForExecution(runContext, frameworkHandle);
            TestLog.Debug("NefTest: RunTests by IEnumerable<string>");

            try {
                var filter = new TestFilter() {
                    Environment = Common.Enums.EnvironmentType.Console,
                    AssemblyFilters = sources.Distinct().ToList(),
                };

                var testResult = _engine.TestRunner.RunAvailableTests(filter);
                _engine.TestReporter.Write(testResult);
            }
            catch (Exception ex) {
                TestLog.Warning($"NefTest: Exception thrown executing tests", ex);
            }

            TestLog.Info($"NefTest: Test execution complete");
        }

        /// <summary>
        /// Called by the VisualStudio IDE when selected tests are to be run. Never called from TFS Build, except (at least 2022, probably also 2019) when vstest.console uses /test: then this is being used.
        /// </summary>
        /// <param name="tests">The tests to be run.</param>
        /// <param name="runContext">The RunContext.</param>
        /// <param name="frameworkHandle">The FrameworkHandle.</param>
        public void RunTests(IEnumerable<Microsoft.VisualStudio.TestPlatform.ObjectModel.TestCase> tests, IRunContext runContext, IFrameworkHandle frameworkHandle) {
            Initialize(runContext, frameworkHandle);
            InitializeForExecution(runContext, frameworkHandle);
            TestLog.Debug($"RunTests by IEnumerable<TestCase>:");

            TestLog.Debug($"Sources: {string.Join(", ", tests.Select(t => t.FullyQualifiedName + " " + t.Source).ToArray())}");

            try {
                var filter = new TestFilter() {
                    Environment = Common.Enums.EnvironmentType.Console,
                    AssemblyFilters = tests.Select(t => t.Source).Distinct().ToList(),
                    TestCaseFilters = tests.Select(t => t.FullyQualifiedName).ToList()
                };

                var testResult = _engine.TestRunner.RunAvailableTests(filter);
                _engine.TestReporter.Write(testResult);
            }
            catch (Exception ex) {
                TestLog.Warning($"NefTest: Exception thrown executing tests", ex);
            }
        }

        void ITestExecutor.Cancel() {
            //StopRun();
        }
        #endregion

        #region Helper Methods
        public void InitializeForExecution(IRunContext runContext, IFrameworkHandle frameworkHandle) {
            TestLog.Info($"NefTest: Test execution started");
            _engine = new NefTestEngine();

            _engine.RegisterComponent(typeof(ILogger), typeof(TestLogger), true, TestLog);
            _engine.RegisterComponent(typeof(ITestFinder), typeof(AdapterTestFinder), true);
            _engine.RegisterComponent(typeof(ITestReporter), typeof(AdapterTestExecutorReporter), true);

            _engine.Initialize();

            frameworkHandle.EnableShutdownAfterTestRun = true;
            ((AdapterTestExecutorReporter)_engine.TestReporter).SetFrameworkHandle(frameworkHandle, ExecutorUri);
        }
        #endregion
    }
}
