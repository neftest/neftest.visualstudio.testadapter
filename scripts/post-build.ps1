#-SolutionDir $(SolutionDir) -ProjectDir $(ProjectDir) -ProjectPath $(ProjectPath) -ConfigurationName $(ConfigurationName) -TargetName $(TargetName) -TargetDir $(TargetDir) -ProjectName $(ProjectName) -PlatformName $(PlatformName) -TargetPath $(TargetPath)

param([string]$SolutionDir,
     [string]$ProjectDir,
     [string]$ProjectPath,
     [string]$ConfigurationName,
     [string]$TargetName,
     [string]$TargetDir,
     [string]$ProjectName,
     [string]$PlatformName,
     [string]$TargetPath);

# imports
. "${SolutionDir}scripts\shared.ps1"

Show-LocalDependencyIssues