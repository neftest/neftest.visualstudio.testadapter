To build locally:

Get [Chocolatey](https://chocolatey.org/install).

```
	choco install nuget.commandline gitversion.portable
	# or have nuget.exe and gitversion.exe in your $Env:PATH
```
